package edu.ntnu.idatt2001.units;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.subclasses.RangedUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RangedTest {
    RangedUnit rangedOne;
    RangedUnit rangedTwo;
    RangedUnit rangedThree;

    @BeforeEach
    public void newRangedUnits() {
        rangedOne = new RangedUnit("Ranged One", 10);
        rangedTwo = new RangedUnit("Ranged Two", 10);
        rangedThree = new RangedUnit("Ranged Three", 10);
    }

    @Test
    public void unitHasDifferentResistBonusOnFirstAndSecondAttack() {
        int resistBonusOne = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonusTwo = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonusThree = rangedOne.getResistBonus(Terrain.PLAINS);
        int resistBonusFour = rangedOne.getResistBonus(Terrain.PLAINS);

        assertTrue(resistBonusOne > resistBonusTwo);
        assertTrue(resistBonusTwo > resistBonusThree);
        assertEquals(resistBonusThree, resistBonusFour);
    }

    @Test
    public void unitsHasMoreAttackBonusInHillsAndLessInForest() {
        int attackBonusPlains = rangedOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusForest = rangedTwo.getAttackBonus(Terrain.FOREST);
        int attackBonusHills = rangedThree.getAttackBonus(Terrain.HILLS);

        assertTrue(attackBonusHills > attackBonusPlains);
        assertTrue(attackBonusPlains > attackBonusForest);
    }

}
