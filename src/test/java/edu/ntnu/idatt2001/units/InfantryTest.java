package edu.ntnu.idatt2001.units;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.subclasses.InfantryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class InfantryTest {
    InfantryUnit infantryOne;
    InfantryUnit infantryTwo;
    InfantryUnit infantryThree;

    @BeforeEach
    public void newInfantryUnits() {
        infantryOne = new InfantryUnit("Infantry One", 10);
        infantryTwo = new InfantryUnit("Infantry Two", 10);
        infantryThree = new InfantryUnit("Infantry Three", 10);
    }

    @Test
    public void unitHasSameAttackBonus() {
        int attackBonusOne = infantryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusTwo = infantryOne.getAttackBonus(Terrain.PLAINS);

        assertEquals(attackBonusOne, attackBonusTwo);
    }

    @Test
    public void unitHasSameResistBonus() {
        int resistBonusOne = infantryOne.getResistBonus(Terrain.PLAINS);
        int resistBonusTwo = infantryOne.getResistBonus(Terrain.PLAINS);

        assertEquals(resistBonusOne, resistBonusTwo);
    }

    @Test
    public void unitsHasMoreAttackBonusInForest() {
        int attackBonusPlains = infantryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusForest = infantryTwo.getAttackBonus(Terrain.FOREST);
        int attackBonusHills = infantryThree.getAttackBonus(Terrain.HILLS);

        assertTrue(attackBonusForest > attackBonusPlains);
        assertEquals(attackBonusHills, attackBonusPlains);
    }

    @Test
    public void unitsHasMoreResistBonusInForest() {
        int resistBonusPlains = infantryOne.getResistBonus(Terrain.PLAINS);
        int resistBonusForest = infantryTwo.getResistBonus(Terrain.FOREST);
        int resistBonusHills = infantryThree.getResistBonus(Terrain.HILLS);

        assertTrue(resistBonusForest > resistBonusPlains);
        assertEquals(resistBonusHills, resistBonusPlains);
    }
}
