package edu.ntnu.idatt2001.units;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.subclasses.CavalryUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CavalryTest {
    CavalryUnit cavalryOne;
    CavalryUnit cavalryTwo;
    CavalryUnit cavalryThree;

    @BeforeEach
    public void newCavalryUnits() {
        cavalryOne = new CavalryUnit("Cavalry One", 10);
        cavalryTwo = new CavalryUnit("Cavalry Two", 10);
        cavalryThree = new CavalryUnit("Cavalry Three", 10);
    }

    @Test
    public void unitHasDifferentAttackBonusOnFirstAttack() {
        int attackBonusOne = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusTwo = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusThree = cavalryOne.getAttackBonus(Terrain.PLAINS);

        assertTrue(attackBonusOne > attackBonusTwo);
        assertEquals(attackBonusThree, attackBonusTwo);
    }

    @Test
    public void unitsHasMoreAttackBonusOnPlains() {
        int attackBonusPlains = cavalryOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusForest = cavalryTwo.getAttackBonus(Terrain.FOREST);
        int attackBonusHills = cavalryThree.getAttackBonus(Terrain.HILLS);

        assertTrue(attackBonusHills < attackBonusPlains);
        assertEquals(attackBonusHills, attackBonusForest);
    }

    @Test
    public void unitsHasLessResistBonusInForest() {
        int resistBonusPlains = cavalryOne.getResistBonus(Terrain.PLAINS);
        int resistBonusForest = cavalryTwo.getResistBonus(Terrain.FOREST);
        int resistBonusHills = cavalryThree.getResistBonus(Terrain.HILLS);

        assertTrue(resistBonusForest < resistBonusPlains);
        assertEquals(resistBonusHills, resistBonusPlains);
    }
}
