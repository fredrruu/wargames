package edu.ntnu.idatt2001.units;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.subclasses.CommanderUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CommanderTest {
    CommanderUnit commanderOne;
    CommanderUnit commanderTwo;
    CommanderUnit commanderThree;

    @BeforeEach
    public void newCommanderUnits() {
        commanderOne = new CommanderUnit("Commander One", 10);
        commanderTwo = new CommanderUnit("Commander Two", 10);
        commanderThree = new CommanderUnit("Commander Three", 10);
    }

    @Test
    public void unitHasDifferentAttackBonusOnFirstAttack() {
        int attackBonusOne = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusTwo = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusThree = commanderOne.getAttackBonus(Terrain.PLAINS);

        assertTrue(attackBonusOne > attackBonusTwo);
        assertEquals(attackBonusThree, attackBonusTwo);
    }

    @Test
    public void unitsHasMoreAttackBonusOnPlains() {
        int attackBonusPlains = commanderOne.getAttackBonus(Terrain.PLAINS);
        int attackBonusForest = commanderTwo.getAttackBonus(Terrain.FOREST);
        int attackBonusHills = commanderThree.getAttackBonus(Terrain.HILLS);

        assertTrue(attackBonusHills < attackBonusPlains);
        assertEquals(attackBonusHills, attackBonusForest);
    }

    @Test
    public void unitsHasLessResistBonusInForest() {
        int resistBonusPlains = commanderOne.getResistBonus(Terrain.PLAINS);
        int resistBonusForest = commanderTwo.getResistBonus(Terrain.FOREST);
        int resistBonusHills = commanderThree.getResistBonus(Terrain.HILLS);

        assertTrue(resistBonusForest < resistBonusPlains);
        assertEquals(resistBonusHills, resistBonusPlains);
    }
}
