package edu.ntnu.idatt2001;

import edu.ntnu.idatt2001.battle.Army;
import edu.ntnu.idatt2001.battle.Battle;
import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.subclasses.InfantryUnit;
import edu.ntnu.idatt2001.battle.units.Unit;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BattleTest {

    @Test
    @DisplayName("Test the Battle simulation")
    void simulate() {

        List<Unit> bigUnits = new ArrayList<>();
        List<Unit> smallUnits = new ArrayList<>();

        Army bigArmy = new Army("Big Bois", bigUnits);
        Army smallArmy = new Army("Small Bois", smallUnits);

        for (int i = 0; i < 100; i++){
            InfantryUnit infantry = new InfantryUnit("Big Inf", 20);
            bigArmy.add(infantry);
        }

        for (int j = 0; j < 50; j++) {
            InfantryUnit smallInf = new InfantryUnit("Small Inf", 15);
            smallUnits.add(smallInf);
        }

        Battle test = new Battle(bigArmy, smallArmy, Terrain.PLAINS);

        assertEquals(bigArmy, test.simulate());
    }

}