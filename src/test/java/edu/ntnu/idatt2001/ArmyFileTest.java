package edu.ntnu.idatt2001;

import static org.junit.jupiter.api.Assertions.*;

import edu.ntnu.idatt2001.battle.Army;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import edu.ntnu.idatt2001.battle.units.Unit;
import edu.ntnu.idatt2001.battle.units.subclasses.CavalryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.CommanderUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.InfantryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.RangedUnit;
import edu.ntnu.idatt2001.filehadling.ArmyFile;
import edu.ntnu.idatt2001.filehadling.FileFormatException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

public class ArmyFileTest {

    @TempDir
    public Path tempDir;

    Army army;
    ArmyFile armyFile;

    @BeforeEach
    public void createNewArmyWithMultipleUnitsOfAllClasses() {
        army = new Army("Army");

        ArrayList<Unit> units = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            units.add(new InfantryUnit("infantry", 15));
            units.add(new RangedUnit("ranged", 10));
            units.add(new CavalryUnit("cavalry", 15));
            units.add(new CommanderUnit("commander", 20));
        }
        army.addAll(units);
        armyFile = new ArmyFile();
    }

    @Nested
    public class WriteArmyToFile {

        @Test
        public void WritingToFileWorks() {
            Path filePath = tempDir.resolve("aName.csv");

            assertDoesNotThrow(() -> armyFile.writeToFile(army, filePath.toString()));
            assertTrue(Files.exists(filePath));
        }

        @Test
        public void WritingToFileWhenArmyDoesntExist() {
            Path filePath = tempDir.resolve("aName.csv");

            assertThrows(NullPointerException.class, () -> armyFile.writeToFile(null, filePath.toString()));
        }

        @Test
        public void WritingToFileWhenFileIsInWrongFormat() {
            Path filePath = tempDir.resolve("aName.cs");

            assertThrows(FileFormatException.class, () -> armyFile.writeToFile(army, filePath.toString()));
        }

        @Test
        public void WritingToFileWorksAndReadArmyIsEqual() {
            ArmyFile armyFile = new ArmyFile();
            Path filePath = tempDir.resolve("aName.csv");
            Army readArmy = new Army("readArmy");

            try {
                armyFile.writeToFile(army, filePath.toString());
                readArmy = armyFile.readFromFile(filePath.toString());
            } catch (IOException e) {
                fail();
            }
            assertEquals(readArmy.getAllUnits().size(), army.getAllUnits().size());
            assertEquals(readArmy.getName(), army.getName());
        }
    }

    @Nested
    public class ReadInvalidFiles {
        String filePath;

        @BeforeEach
        public void writeToFile() {
            ArmyFile armyFile = new ArmyFile();
            filePath = tempDir.resolve("aName.csv").toString();
            try{
                armyFile.writeToFile(army, filePath);
            } catch (IOException e) {
                fail();
            }
        }

        @Test
        public void fileReadsOnlyValidLinesInAFileWithBadLines() {
            ArmyFile armyFile = new ArmyFile();

            try(FileWriter fileWriter = new FileWriter(filePath, true)) {
                fileWriter.write("RangedUnit,bob,-2\n");
                fileWriter.write("Unit,bob,10\n");
                fileWriter.write("RangedUnit,bob,one\n");
                fileWriter.write("RangedUnit,ranged,1.0\n");
                fileWriter.write("InfantryUnit,bob,10\n");
                fileWriter.write("RangedUnit\n");
            } catch (IOException e) {
                fail();
            }

            int armySize = army.getAllUnits().size();
            int infantryUnitSize = army.getInfantryUnits().size();

            try {
                Army readArmy = armyFile.readFromFile(filePath);

                assertEquals(armySize + 1, readArmy.getAllUnits().size());
                assertEquals(infantryUnitSize + 1, readArmy.getInfantryUnits().size());
                assertEquals(5, armyFile.getInvalidLines().size());
            } catch (IOException e) {
                fail();
            }
        }
    }
}