package edu.ntnu.idatt2001.filehadling;

import edu.ntnu.idatt2001.battle.Army;
import edu.ntnu.idatt2001.battle.units.Unit;
import edu.ntnu.idatt2001.battle.units.UnitFactory;
import edu.ntnu.idatt2001.battle.units.UnitType;
import javafx.stage.FileChooser;


import java.util.ArrayList;
import java.util.List;
import java.io.*;

/**
 *
 */
public class ArmyFile {

    private final List<String> invalidLines;

    public ArmyFile() {
        invalidLines = new ArrayList<>();
    }

    public List<String> getInvalidLines() {
        return invalidLines;
    }

    public void writeToFile(Army army, String path) throws NullPointerException, FileFormatException, IOException {
        if (army == null) throw new NullPointerException("Army is empty");
        if (!path.endsWith(".csv")) throw new FileFormatException("File format needs to be .csv");

        try (FileWriter fileWriter = new FileWriter(path)) {
            fileWriter.write(army.getName() + "\n");
            for (Unit unit : army.getAllUnits()) {
                fileWriter.write(unit.getClassName() + "," + unit.getName() + "," +unit.getHealth() + "\n");
            }
        }
    }

    public Army readFromFile(String path) throws FileNotFoundException, IllegalArgumentException, IOException {
        if (!path.endsWith(".csv")) throw new FileFormatException("File format needs to be .csv");

        UnitFactory unitFactory = new UnitFactory();

        try(BufferedReader br = new BufferedReader(new FileReader(path))) {
            String line;
            Army army = new Army(br.readLine());
            while((line = br.readLine()) != null) {
                try {
                    String[] unit = line.split(",");
                    army.add(unitFactory.newUnit(UnitType.getUnitType(unit[0].trim()),
                            unit[1].trim(), Integer.parseInt(unit[2].trim()) ));
                } catch (NumberFormatException e) {
                    invalidLines.add("Line # \"" + line + "\" skipped because health is not a valid number \"" + e.getMessage() + " \"");
                } catch (IllegalArgumentException e) {
                    invalidLines.add("Line # \"" + line + "\" skipped because \"" + e.getMessage() + " \"");
                } catch (ArrayIndexOutOfBoundsException e) {
                    invalidLines.add("Line # \"" + line + "\" skipped because it lacked information");
                }
            }
            return army;
        }
    }
}
