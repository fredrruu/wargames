package edu.ntnu.idatt2001.filehadling;

/**
 * Thows a exeption if the file format is wrong
 *
 * @author Fredrik Ruud
 */
public class FileFormatException extends IllegalArgumentException {

    /**
     * creates an exception with an error message
     * @param error error message with details of the error
     */
    public FileFormatException(String error) {
        super(error);
    }
}
