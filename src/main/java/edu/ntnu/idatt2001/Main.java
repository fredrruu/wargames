package edu.ntnu.idatt2001;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import java.util.Objects;

/**
 * Class that extends application from javaFX
 * Creates the stage for the GUI
 *
 * @author Fredrik Ruud
 */
public class Main extends Application {
    public static Stage stage;
    @Override
    public void start(Stage stage) throws Exception {

        Main.stage = stage;
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        stage.setX(bounds.getMinX());
        stage.setY(bounds.getMinY());
        stage.setWidth(bounds.getWidth());
        stage.setHeight(bounds.getHeight());
        stage.getIcons().add(new Image(Objects.requireNonNull(Main.class.getResourceAsStream("/images/logo.png"))));
        FXMLLoader fxmlLoader = new FXMLLoader(Main.class.getResource("/view/Frontpage.fxml"));
        Scene scene = new Scene(fxmlLoader.load(),stage.getWidth(),stage.getHeight());
        stage.setTitle("JavaFX Scene");
        stage.setScene(scene);
        stage.show();

    }
    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void Main(String[] args) {
        launch();
    }
}
