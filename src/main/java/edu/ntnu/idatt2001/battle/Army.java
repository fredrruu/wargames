package edu.ntnu.idatt2001.battle;

import edu.ntnu.idatt2001.battle.units.*;
import edu.ntnu.idatt2001.battle.units.subclasses.CavalryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.CommanderUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.InfantryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.RangedUnit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;

/**
 * This is the class Army.
 * @author Fredrik Ruud
 */
public class Army {
    private final String name;
    private List<Unit> units = new ArrayList<>();
    Random rand = new Random();

    /**
     * Creates a new empty army.
     * @param name the name of the army.
     */
    public Army(String name) {
        this.name = name;
    }

    /**
     * Creates a new empty army.
     * @param name the name of the army.
     * @param units A list of units.
     */
    public Army(String name, List<Unit> units) throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be empty");
        if (name.contains(",")) throw new IllegalArgumentException("Name cannot contain a comma");

        this.name = name;
        this.units = units;
    }

    /**
     * Get the name of the army.
     * @return the name of the army.
     */
    public String getName() throws IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name cannot be empty");
        if (name.contains(",")) throw new IllegalArgumentException("Name cannot contain a comma");

        return name;
    }

    /**
     * Add a unit to the army.
     * @param unit the unit you want to add.
     */
    public void add(Unit unit){
        units.add(unit);
    }

    /**
     * Add a list of units to the army.
     * @param units the list of units you want to add.
     */
    public void addAll(List<Unit> units){
        for (Unit unit : units) {
            add(unit);
        }
    }

    /**
     * Removes a Unit from the army.
     * @param unit The unit that will be removed.
     */
    public void remove(Unit unit){
        units.remove(unit);
    }

    /**
     * A check to see if an army has units in it or not.
     * @return true or false if the army has a unit or not.
     */
    public boolean hasUnits(){
        return units.size() != 0;
    }

    /**
     * Returns a list of infantry units
     *
     * @return a list of infantry units
     */
    public List<Unit> getInfantryUnits() {
        return units.stream().filter(n -> n instanceof InfantryUnit).toList();
    }

    /**
     * Returns a list of infantry units
     *
     * @return a list of infantry units
     */
    public List<Unit> getRangedUnits() {
        return units.stream().filter(n -> n instanceof RangedUnit).toList();
    }

    /**
     * Returns a list of infantry units
     *
     * @return a list of calvary units
     */
    public List<Unit> getCalvaryUnits() {
        return units.stream().filter(n -> n instanceof CavalryUnit && !(n instanceof CommanderUnit)).toList();
    }

    /**
     * Returns a list of commander units
     *
     * @return a list of commander units
     */
    public List<Unit> getCommanderUnits() {
        return units.stream().filter(n -> n instanceof CommanderUnit).toList();
    }

    /**
     * Gives you a list of all the units in an army.
     * @return all the units in the army.
     */
    public List<Unit> getAllUnits() {
        return units;
    }

    /**
     * Get a random unit from the army.
     * @return an random unit from the army.
     */
    public Unit getRandom() {
        return units.get(rand.nextInt(units.size()));
    }

    /**
     * Get a string of info about the army.
     * @return a string of info about the army.
     */
    @Override
    public String toString() {
        int infantryUnits = 0;
        int rangedUnits = 0;
        int cavalryUnits = 0;
        int commanderUnits = 0;

        for (Unit unit : units) {
            if (unit instanceof InfantryUnit) infantryUnits++;
            else if (unit instanceof RangedUnit) rangedUnits++;
            else if (unit instanceof CavalryUnit && !(unit instanceof CommanderUnit)) cavalryUnits++;
            else commanderUnits++;
        }
        return ("Army " + name + '\n' +
                "----------------------\n" +
                "Infantry: " + infantryUnits + "\n" +
                "Ranged: " + rangedUnits + "\n" +
                "Cavalry: " + cavalryUnits + "\n" +
                "Commander: " + commanderUnits + "\n" +
                "----------------------\n"
        );
    }

    /**
     * A test to compare armies .
     * @param o the object you want to compare.
     * @return True if the objects match, false if they don't.
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army army)) return false;
        return name.equals(army.name) && units.equals(army.units);
    }

    /**
     * Gets a hashCode
     * @return a hashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
