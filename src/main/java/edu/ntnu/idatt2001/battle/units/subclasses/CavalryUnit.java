package edu.ntnu.idatt2001.battle.units.subclasses;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.Unit;

/**
 * This is the class CavalryUnit.
 * It is based on the Superclass Unit.
 * @author Fredrik Ruud
 */
public class CavalryUnit extends Unit {

    /**
     * Creates a new Cavalry unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     * @param attack attack of the unit.
     * @param armor armor of the unit.
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new Cavalry unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     *               attack is 20.
     *               armour is 12.
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }

    /**
     * Constants that are used to calculate the attack and resist bonus for a unit
     * first attack bonus is set for the charge and first attack bonus
     * melee attack bonus is the base attack bonus after the unit has attacked once
     * resist bonus is the base resist number
     * plains bonus is used to increase the attack value of the cavalry unit if it is attacking on plains
     * forest bonus is used to lower the resist of the unit if it atackes in a forest
     */
    private int firstAttackBonus = 6;
    private final int MELEE_ATTACK_BONUS = 2;
    private final int RESIST_BONUS = 2;
    private final int PLAINS_BONUS = 6;
    private final int FOREST_BONUS = 0;

    /**
     * The override for getAttackBonus.
     *
     * If the unit hasn't attacked it gets a higher first attack bonus
     * after that, attack bonus is set to the melee attack bonus
     * If the battle is on a plane an additional planes bonus is applied
     *
     * @return attackBonus
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = firstAttackBonus;
        firstAttackBonus = MELEE_ATTACK_BONUS;

        if(terrain == Terrain.PLAINS) attackBonus += PLAINS_BONUS;
        return attackBonus;
    }

    /**
     * The override for getResistBonus.
     *
     * If the unit attackes in a forest the resist is set to 0
     * If the unit attackes anywhere else resist is set to 1
     *
     * @return resist
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? FOREST_BONUS : RESIST_BONUS;
    }
}
