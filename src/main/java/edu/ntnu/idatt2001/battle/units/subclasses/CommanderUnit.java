package edu.ntnu.idatt2001.battle.units.subclasses;

/**
 * This is the class CommanderUnit.
 * It is based on the Cavalry unit.
 * @author Fredrik Ruud
 */
public class CommanderUnit extends CavalryUnit{

    /**
     * Creates a new Commander unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     * @param attack attack of the unit.
     * @param armor armor of the unit.
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new Commander unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     *               attack is 25.
     *               armour is 15.
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }
}
