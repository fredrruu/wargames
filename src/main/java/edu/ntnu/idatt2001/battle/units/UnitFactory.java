package edu.ntnu.idatt2001.battle.units;


import edu.ntnu.idatt2001.battle.units.subclasses.CavalryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.CommanderUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.InfantryUnit;
import edu.ntnu.idatt2001.battle.units.subclasses.RangedUnit;

import java.util.ArrayList;
import java.util.List;

/**
 * A class to create units, you can create either one unit or many at a time
 *
 * @author Fredrik Ruud
 */
public class UnitFactory {

    public static Unit newUnit(UnitType unitType, String name, int health) throws IllegalArgumentException {
        if ( unitType == UnitType.CAVALRY_UNIT) return new CavalryUnit(name, health);
        if ( unitType == UnitType.RANGED_UNIT) return new RangedUnit(name, health);
        if ( unitType == UnitType.INFANTRY_UNIT) return new InfantryUnit(name, health);
        if ( unitType == UnitType.COMMANDER_UNIT) return new CommanderUnit(name, health);

        throw new IllegalArgumentException("Unit type does not exist");
    }

    public static List<Unit> newListOfUnits(int numberOfUnits, UnitType unitType, String name, int health) throws IllegalArgumentException {
        if (numberOfUnits < 0 ) throw new IllegalArgumentException("The number of units cannot be a negative number");

        List<Unit> units = new ArrayList<>();
        for (int i = 0; i < numberOfUnits; i++) {
            units.add(newUnit(unitType, name, health));
        }
        return units;
    }
}
