package edu.ntnu.idatt2001.battle.units;

import edu.ntnu.idatt2001.battle.Terrain;

/**
 *This is the superclass Unit.
 * @author Fredrik Ruud
 */
public abstract class Unit {
    private final String name;
    private int health;
    private final int attack;
    private final int armor;

    /**
     * Apply values to the units.
     *
     * @param name the name of the Unit.
     * @param health sets the health of the unit.
     * @param attack sets the attack value of the unit.
     * @param armor sets the armour of the unit.
     */
    public Unit(String name, int health, int attack, int armor) throws  IllegalArgumentException {
        if (name.isBlank()) throw new IllegalArgumentException("Name Cannot be empty");
        if (name.contains(",")) throw new IllegalArgumentException("Name cannot contain a comma");
        if (health <= 0) throw new IllegalArgumentException("Health can't be less than or equals to zero");
        if (attack < 0) throw new IllegalArgumentException("Attack cannot be negative");
        if (armor < 0) throw new IllegalArgumentException("Armor cannot be negative");

        this.name = name;
        this.health = health;
        this.attack = attack;
        this.armor = armor;
    }

    /**
     * Calculate the damage an attack deals to the unit f.
     *
     * @param f Another unit.
     * @param terrain The terrain the battle is held at
     */
    public void attack(Unit f, Terrain terrain){
        int damage = this.getAttack() + this.getAttackBonus(terrain) - f.getArmor() - f.getResistBonus(terrain);
        if (damage > 0) {
            f.setHealth(f.getHealth() - damage);
        }
    }

    /**
     * Get the name of the unit.
     *
     * @return the name of the unit.
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the name of the unit class
     *
     * @return name of the unit class
     */
    public String getClassName() {return getClass().getSimpleName();}

    /**
     * Get the health of the unit.
     *
     * @return the health of the unit.
     */
    public int getHealth() {
        return health;
    }

    /**
     * Get the attack of the unit.
     *
     * @return the attack of the unit.
     */
    public int getAttack() {
        return attack;
    }

    /**
     * Get the armour of the unit
     *
     * @return the armor of the unit
     */
    public int getArmor() {
        return armor;
    }

    /**
     * set the health of the unit
     *
     * @param health the new health you want to give the unit
     */
    public void setHealth(int health) {
        this.health = health;
    }

    /**
     * Get info of the unit as a string
     *
     * @return String about the unit
     */
    @Override
    public String toString() {
        return "Unit{" +
                "Name='" + name + '\'' +
                ", health=" + health +
                ", attack=" + attack +
                ", armor=" + armor +
                '}';
    }

    /**
     * An abstract class that changes based on the subclasses
     */
    public abstract int getAttackBonus(Terrain terrain);

    /**
     * An abstract class that changes based on the subclasses
     */
    public abstract int getResistBonus(Terrain terrain);
}
