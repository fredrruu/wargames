package edu.ntnu.idatt2001.battle.units.subclasses;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.Unit;

/**
 * This is the class InfantryUnit.
 * It is based on the Superclass Unit.
 * @author Fredrik Ruud
 */
public class InfantryUnit extends Unit {

    /**
     * Creates a new Infantry unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     * @param attack attack of the unit.
     * @param armor armor of the unit.
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new Infantry unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     *               attack is 15.
     *               armour is 10.
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    /**
     * Constants that are used to calculate the attack and resit bonus for a unit
     * attack bonus is the base attack bonus of the unit
     * resist bonus is the base resist bonus of the unit
     * forrest bonus is applied to the unit when it is fighting in a forest
     */
    private final int ATTACK_BONUS = 1;
    private final int RESIT_BONUS = 1;
    private final int FOREST_BONUS = 4;

    /**
     * The override for getAttackBonus
     * @return 2
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? ATTACK_BONUS + FOREST_BONUS : ATTACK_BONUS;
    }

    /**
     * The override for getResistBonus
     * @return 1
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        return (terrain == Terrain.FOREST) ? RESIT_BONUS + FOREST_BONUS : RESIT_BONUS;
    }
}
