package edu.ntnu.idatt2001.battle.units.subclasses;

import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.Unit;

/**
 * This is the class RangedUnit.
 * It is based on the Superclass Unit.
 * @author Fredrik Ruud
 */
public class RangedUnit extends Unit {

    /**
     * Creates a new Ranged unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     * @param attack attack of the unit.
     * @param armor armor of the unit.
     */
    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Creates a new Ranged unit.
     *
     * @param name name of the unit.
     * @param health health of the unit.
     *               attack is 15.
     *               armour is 8.
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    /**
     * Values used to calculate the attack and resist bonuses of a unit.
     * Attack bonus is the base attack bonus for the unit
     * First resit bonus is the resist bonus the units has the first number of attacks
     * Last resist bonus is the final resist bonus for the unit
     * hills bonus is an extra bonus for attacks on a hill
     * forest bonus is a bonus set for attacks in a forest
     */
    private final int ATTACK_BONUS = 3;
    private final int FIRST_RESIST_BONUS = 6;
    private final int LAST_RESIST_BONUS = -2;
    private final int HILLS_BONUS = 2;
    private final int FOREST_BONUS = -2;
    private int hasResisted = 0;

    /**
     * The override for getAttackBonus.
     * the unit gets a bonus for attacking on hills
     * the unit gets a negative bonus for fighting in the forest
     * @return attackBonus
     */
    @Override
    public int getAttackBonus(Terrain terrain) {
        int attackBonus = ATTACK_BONUS;

        if (terrain == Terrain.HILLS) attackBonus += HILLS_BONUS;
        if (terrain == Terrain.FOREST) attackBonus += FOREST_BONUS;
        return attackBonus;
    }

    /**
     * The override for getResistBonus.
     * when the unit attacks for the first time it has an extra bonus applied
     * this bonus will apply twice and then the resist will be negative as the unit is considered to be
     * close to the attacking units and struggle to battle
     * @return attackBonus
     */
    @Override
    public int getResistBonus(Terrain terrain) {
        int resistBonus = hasResisted >= 2 ? LAST_RESIST_BONUS : FIRST_RESIST_BONUS - 2 * hasResisted;
        hasResisted++;
        return resistBonus;
    }
}
