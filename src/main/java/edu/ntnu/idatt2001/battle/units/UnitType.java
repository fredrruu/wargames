package edu.ntnu.idatt2001.battle.units;


import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * This is an enum that is created to differentiate between the different unit types.
 *
 * @author Fredrik Ruud
 */
public enum UnitType {

    INFANTRY_UNIT("InfantryUnit"),
    RANGED_UNIT("RangedUnit"),
    CAVALRY_UNIT("CavalryUnit"),
    COMMANDER_UNIT("CommanderUnit");

    private final String className;
    private static final Map<String, UnitType> unitTypes = Arrays.stream(UnitType.values())
            .collect(Collectors.toMap(UnitType::getClassName, Function.identity()));

    /**
     * The enum constructor
     *
     * @param className This is the enum in a string format
     */
    UnitType(String className) {
        this.className = className;
    }

    /**
     * Gets the name of the class from the enum
     *
     * @return className
     */
    private String getClassName() {
        return className;
    }

    /**
     * Gets a UnitType from a string
     * If the unit type isn't found, null is returned
     *
     * @param name String
     * @return UnitType
     */
    public static UnitType getUnitType(String name) {
        return unitTypes.getOrDefault(name, null);
    }
}
