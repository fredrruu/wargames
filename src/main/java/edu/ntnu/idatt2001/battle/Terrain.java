package edu.ntnu.idatt2001.battle;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * An enum that determent's the type of terrain that battles will be held at
 *
 * @author Fredrik Ruud
 */
public enum Terrain {
    PLAINS("Plains"),
    FOREST("Forest"),
    HILLS("Hills");

    private final String terrainName;
    private static final Map<String, Terrain> terrains = Arrays.stream(Terrain.values()).collect(Collectors
            .toMap(Terrain::getTerrainName, Function.identity()));

    Terrain(String terrainName) {
        this.terrainName = terrainName;
    }

    public String getTerrainName() {
        return terrainName;
    }

    public static Terrain getTerrain(String terrainName) {
        return terrains.getOrDefault(terrainName, null);
    }
}
