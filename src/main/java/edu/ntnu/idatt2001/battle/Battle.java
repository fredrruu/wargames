package edu.ntnu.idatt2001.battle;

import edu.ntnu.idatt2001.battle.units.Unit;

import java.util.Objects;

/**
 * This is the Battle Class. It simulates battles between two armies
 *
 * @author Fredrik Ruud
 */
public class Battle {
    private final Army armyOne;
    private final Army armyTwo;
    private final Terrain terrain;

    /**
     * The two armies that will do battle
     * @param armyOne army number one
     * @param armyTwo army number two
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) throws IllegalArgumentException, NullPointerException{
        if (armyOne == null || armyTwo == null) throw new NullPointerException("Army(s) is not added");
        if (!armyOne.hasUnits() || !armyTwo.hasUnits()) throw new IllegalArgumentException("Army(s) can not be empty");
        if (Objects.equals(armyOne, armyTwo)) throw new IllegalArgumentException("Armies can not be the same");
        if (terrain == null) throw new IllegalArgumentException("Terrain is not set");

        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }

    /**
     * gets terrain where the battle is held
     * @return terrain
     */
    public Terrain getTerrain() {
        return terrain;
    }
    /**
     * Simulates a battle.
     * Army one starts the battle and a random unit is selected to attack another unit form the other army
     * next another random unit form the other army attacks another random unit from the first army
     * if the health of a unit drops to zero or bellow the unit is removed from the army
     * as long as both armies has a unit the battle continues one attack at a time
     * @return The army that still has units after a battle.
     */
    public Army simulate() {
        int turn = 0;
        while (armyOne.hasUnits() && armyTwo.hasUnits()){
            Unit unit1 = armyOne.getRandom();
            Unit unit2 = armyTwo.getRandom();
            if (turn %  2 == 0){

                unit1.attack(unit2, terrain);
                if (unit2.getHealth() <= 0){
                    armyTwo.remove(unit2);
                }

            }else {

                unit2.attack(unit1, terrain);
                if (unit1.getHealth() <= 0){
                    armyOne.remove(unit1);
                }
            }
            turn ++;
        }
        if (armyOne.hasUnits()){
            return armyOne;
        }else {
            return armyTwo;
        }
    }

    /**
     * String with info about the battle.
     */
    @Override
    public String toString() {
        if (armyOne.hasUnits() && armyTwo.hasUnits()) {
            return "Army one:\n" + armyOne + "\n Army two: \n" + armyTwo + "\n";
        }
        if (!armyOne.hasUnits() && armyTwo.hasUnits()) {
            return "Winner!!!\n The surviving army is:\n" +armyTwo;
        }
        return "Winner!!!\n The surviving army is:\n" +armyOne;
    }
}
