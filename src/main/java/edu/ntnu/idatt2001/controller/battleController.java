package edu.ntnu.idatt2001.controller;

import edu.ntnu.idatt2001.Main;
import edu.ntnu.idatt2001.battle.Army;
import edu.ntnu.idatt2001.battle.Battle;
import edu.ntnu.idatt2001.battle.Terrain;
import edu.ntnu.idatt2001.battle.units.Unit;
import edu.ntnu.idatt2001.dialogs.Dialogs;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.stage.Stage;

import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.ResourceBundle;

/**
 * Contoller for the Battle page
 */
public class battleController implements Initializable {

    @FXML
    private Label Results;

    @FXML
    private Label armyOneName;

    @FXML
    private Label armyTwoName;

    @FXML
    private ListView<Unit> armyOneUnits, armyTwoUnits;

    @FXML
    private ChoiceBox<String> terrainChoiceBox = new ChoiceBox<>();

    /**
     * objects that will be used later
     */
    Army army1;
    Army army2;

    /**
     * Initialises the scene
     * @param url
     * @param resourceBundle
     */
    public void initialize(URL url, ResourceBundle resourceBundle) {
        updateTables();
        Arrays.stream(Terrain.values()).forEach(s -> terrainChoiceBox.getItems().add(s.getTerrainName()));
        terrainChoiceBox.setValue(Terrain.PLAINS.getTerrainName());
    }

    /**
     * used to transfer data between scenes
     * @param army1 army one
     * @param army2 army two
     */
    public void receveArmyData(Army army1, Army army2) {
        this.army1 = army1;
        this.army2 = army2;
        updateTables();
        if (army1.hasUnits()) armyOneName.setText(army1.getName());
        if (army2.hasUnits()) armyTwoName.setText(army2.getName());
    }

    /**
     * Moves scene to edit army one
     */
    @FXML
    void editArmyOne() {
        loadEditArmyScene(1, army1, army2);
    }

    /**
     * Moves scene to edit army two
     */
    @FXML
    void editArmyTwo() {
        loadEditArmyScene(2, army2, army1);
    }

    /**
     * The function that helpes move data to the edit army scene
     * @param armyIndex    what index the army is
     * @param selectedArmy the army that is going to be edited in the next scene
     * @param otherArmy    the other army, nothing will happen to it in the next scene
     */
    public void loadEditArmyScene(int armyIndex, Army selectedArmy, Army otherArmy) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/editArmy.fxml"));
        try {
            Parent root = loader.load();
            editArmyController controller = loader.getController();
            controller.receiveArmyData(armyIndex, selectedArmy, otherArmy);

            Stage stage = Main.stage;
            stage.getScene().setRoot(root);
        } catch (IOException e) {
            Dialogs.showAlertDialog(e);
        }
    }

    /**
     * runs the battle simulation
     */
    @FXML
    public void runSimulation() {
        if(army1 != null && army2 != null) {
            if (army1.hasUnits() && army2.hasUnits()) {
                if(Dialogs.showConfirmationDialog("Do you want to start the game? ")){
                    Battle simulate = new Battle(army1, army2, Terrain.getTerrain(terrainChoiceBox.getValue()));
                    Results.setText(simulate.simulate().toString());
                }
            } else{
                Dialogs.showAlertDialog("Please make sure that both armies have units");
            }
        }
        else{
            Dialogs.showAlertDialog("Please make / import both armies");
        }
    }

    /**
     * updates the tables that stores the army units
     */
    public void updateTables() {
        armyOneUnits.getItems().clear();
        armyOneUnits.getItems().addAll(army1.getAllUnits());
        armyTwoUnits.getItems().clear();
        armyTwoUnits.getItems().addAll(army2.getAllUnits());
    }

    /**
     * a function used to move out from the edit army scene
     * @param selectedArmy is the army that has just been changed
     * @param otherArmy    is the same army is in the load edit army scene
     */
    public static void loadBattleContollerScene(Army selectedArmy, Army otherArmy) {
        FXMLLoader loader = new FXMLLoader(battleController.class.getResource("/view/Frontpage.fxml"));
        try {
            Parent root = loader.load();
            battleController controller = loader.getController();
            controller.receveArmyData(selectedArmy, otherArmy );

            Stage stage = Main.stage;
            stage.getScene().setRoot(root);
        }catch (IOException e){
            Dialogs.showAlertDialog(e);
        }
    }

}
