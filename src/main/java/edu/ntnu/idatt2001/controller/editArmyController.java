package edu.ntnu.idatt2001.controller;

import edu.ntnu.idatt2001.Main;
import edu.ntnu.idatt2001.battle.Army;
import edu.ntnu.idatt2001.battle.units.Unit;
import edu.ntnu.idatt2001.battle.units.UnitFactory;
import edu.ntnu.idatt2001.battle.units.UnitType;
import edu.ntnu.idatt2001.dialogs.Dialogs;
import edu.ntnu.idatt2001.filehadling.ArmyFile;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Contoller for the edit army page
 */
import static java.lang.Integer.parseInt;

public class editArmyController {
    @FXML
    private Text updateTitle;

    @FXML
    private TableView<Unit> armyTableView;

    @FXML
    private Label fileUploadedFromText;

    @FXML
    private TextField inputAmount, inputHealth, inputUnitName, nameInput;

    @FXML
    private TableColumn<?, ?> healthCol;

    @FXML
    private TableColumn<?, ?> nameCol;

    @FXML
    private TableColumn<?, ?> typeCol;

    @FXML
    private ComboBox<String> unitTypeComboBox = new ComboBox<>();

    /**
     * set up some objects for later use
     */
    private Army army1;
    private Army army2;
    private int armyIndex;

    /**
     * The Units in the army.
     */
    ArrayList<Unit> unitsInArmy = new ArrayList<>();


    /**
     * Receive army information from the frontpage scene.
     *
     * @param armyIndex    the army index
     * @param selectedArmy the selected army
     * @param otherArmy    the other army gets transferred as well to keep it's values for later use
     */
    public void receiveArmyData(int armyIndex, Army selectedArmy, Army otherArmy ){
        unitsInArmy = new ArrayList<>();
        if(armyIndex==1){
            this.army1 = selectedArmy;
            this.army2 = otherArmy;
        } else{
            this.army2 = selectedArmy;
            this.army1 = otherArmy;
        }
        this.armyIndex=armyIndex;
        updateTitle.setText("Updating Army" + armyIndex);
        if(selectedArmy!=null){
            unitsInArmy.addAll(selectedArmy.getAllUnits());
            nameInput.setText(selectedArmy.getName());
        }
        updateTable();
        addTypesToComboBox();
    }

    /**
     * saveArmyToFile saves an army to a folder nested in the resources folder
     */
    @FXML
    void saveArmyToFile(){
        ArmyFile fileWriter = new ArmyFile();
        String name = nameInput.getText();
        try {
            Army army = new Army(name, unitsInArmy);
            try {
                fileWriter.writeToFile(army, "src/main/resources/Armies/" + name + ".csv");
                fileUploadedFromText.setText("File Saved to src/main/resources/Armies/" + name + ".csv");
            } catch (IOException e) {
                Dialogs.showAlertDialog("Army could not be saved", e);
            }
        } catch (IllegalArgumentException e) {
            Dialogs.showAlertDialog(e);
        }
    }

    /**
     * Add Army
     * Uses the inputFields to create units and add them to the table.
     */
    @FXML
    public void addArmy() {
        String type = unitTypeComboBox.getValue();
        String name = inputUnitName.getText();
        try {
            int health = parseInt(inputHealth.getText());
            int amount = parseInt(inputAmount.getText());
            try {
                unitsInArmy.addAll(UnitFactory.newListOfUnits(amount, UnitType.valueOf(type), name, health));
            }catch (IllegalArgumentException e){
                Dialogs.showAlertDialog("Units could not be added to army", e);
            } catch (NullPointerException e){
                Dialogs.showAlertDialog("Units could not be added", e);
            }
            updateTable();
        }catch (NumberFormatException e){
            Dialogs.showAlertDialog("health and amount must be integers", e);
        }
    }

    /**
     * Upload army from file
     * Creates an army based on the information from a CSV-file
     */
    @FXML
    void uploadArmyFromFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select an army File");
        fileChooser.getExtensionFilters().add(
                new FileChooser.ExtensionFilter("CSV","*.csv")
        );
        fileChooser.setInitialDirectory(new File("src/main/resources/Armies/"));
        try{
            File selectedFile = fileChooser.showOpenDialog(Main.stage);
            ArmyFile fileWriter = new ArmyFile();
            if(armyIndex==1){
                army1 = fileWriter.readFromFile(selectedFile.getPath());
                receiveArmyData(armyIndex,army1,army2);
            }
            else{
                army2 = fileWriter.readFromFile(selectedFile.getPath());
                receiveArmyData(armyIndex,army2,army1);
            }
            fileUploadedFromText.setText("File uploaded from: " + selectedFile.getPath());
        } catch (NullPointerException e){
            Dialogs.showAlertDialog("You did not choose a file");
        } catch (NumberFormatException | IOException e){
            Dialogs.showAlertDialog(e);
        } catch (IllegalArgumentException e){
            Dialogs.showAlertDialog(e.getMessage());
        }
    }

    /**
     * save army
     * Converts the added units and the inputted name into an army and changes scene back to the battle controller.
     */
    @FXML
    void saveArmy() {
        try {
            if (armyIndex == 1) {
                army1 = new Army(nameInput.getText(), unitsInArmy);
            } else {
                army2 = new Army(nameInput.getText(), unitsInArmy);
            }
            battleController.loadBattleContollerScene(army1,army2);
        }catch (IllegalArgumentException e){
            Dialogs.showAlertDialog(e);
        }
    }

    /**
     * Update table
     * Fills the table with units
     */
    public void updateTable() {
        typeCol.setCellValueFactory( new PropertyValueFactory<>("type"));
        nameCol.setCellValueFactory( new PropertyValueFactory<>("name"));
        healthCol.setCellValueFactory( new PropertyValueFactory<>("health"));
        ObservableList<Unit> unitObservableList = FXCollections.observableArrayList(unitsInArmy);
        armyTableView.setItems(unitObservableList);
    }

    private void addTypesToComboBox() {
        ObservableList<String> typeList =
                FXCollections.observableArrayList("InfantryUnit", "RangedUnit", "CavalryUnit", "CommanderUnit");
        unitTypeComboBox.setItems(typeList);
    }

    /**
     * Back button clicked. Changes scene to the frontpage without converting added units and army name.
     * If the army is imported, it will still be transferred to the battleScene
     */
    @FXML
    public void back(){
        if(Dialogs.showConfirmationDialog("Go back to main menu? Changes may not have been saved ")){
            battleController.loadBattleContollerScene(army1,army2);
        }
    }
    @FXML
    public void clearTable(){
        unitsInArmy = new ArrayList<>();
        fileUploadedFromText.setText("");
        updateTable();
    }
}
